function fix_legend_fonts(hl, hobj, ms)
%% Syntax:
%  fix_legend_fonts(hl)
%  fix_legend_fonts(hl, ms)
% Input:
%  hl - legend handle
%  ms - marker size
%
 
fs = 13;
it = 'latex';
ft = 'times';
hl.FontSize = fs;
hl.FontName = ft;
hl.Interpreter = it;
if nargin >= 3
   legendmarkeradjust(hobj, ms)
end

function legendmarkeradjust(hobj, marksize)
fs = 13;
it = 'latex';
ft = 'times';     
type = arrayfun(@(i) hobj(i).Type, 1:length(hobj),'un',0);
for m=1:length(type)
   if strcmp(type{m},'hggroup')
      hobj(m).Children.MarkerSize = marksize;
   elseif strcmp(type{m},'text')
      hobj(m).FontSize = fs;
      hobj(m).FontName = ft;
      hobj(m).Interpreter = it;
   end
end