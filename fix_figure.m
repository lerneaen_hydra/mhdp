function fix_figure(h, sizeRatio, sizeBase)
%FIX_FIGURE Applies a set of basic configuration/set-up to a figure

if nargin <= 1
   sizeRatio = [4,3];
end
if nargin <= 2
	sizeBase = 120;
end

figure(h);

fs = 14;
fsl = 16;
fst = 18;
sz = sizeBase*sizeRatio;
it = 'latex';
ft = 'times';

if(isempty(h.Children))
	hold on;	%Hacky way to create axes for empty figure
end

drawnow;
%Scan all children and apply identical settings to all Axes children
for i=1:length(h.Children)
	if(isa(h.Children(i), 'matlab.graphics.axis.Axes'))
		ha = h.Children(i);
		hold(ha, 'on');
		grid(ha, 'on');
		box(ha, 'on');
		ha.FontSize = fs;
		ha.FontName = ft;
		ha.LabelFontSizeMultiplier = fsl/fs;
		ha.TitleFontSizeMultiplier = fst/fs;
        ha.TickLabelInterpreter = 'latex';
		tp = get(gcf,'Position');
		tp([3,4]) = sz;
		set(gcf,'Position',tp)
		movegui(gcf,'onscreen')
		ha.XLabel.FontName = ft;
		ha.XLabel.Interpreter = it;
		ha.YLabel.FontName = ft;
		ha.YLabel.Interpreter = it;
		ha.ZLabel.FontName = ft;
		ha.ZLabel.Interpreter = it;
		ha.Title.FontName = ft;
		ha.Title.Interpreter = it;
		
		%It's possible this child has been generated using yyaxis, if so
		%check and manage this
		if(length(ha.YAxis) > 1)
			for k=1:length(ha.YAxis)
				ha.YAxis(k).Label.FontSize = fs;
				ha.YAxis(k).Label.FontName = ft;
				ha.YAxis(k).Label.Interpreter = it;
			end
		end
		
    elseif(isa(h.Children(i), 'matlab.graphics.illustration.ColorBar'))
        ha = h.Children(i);
        ha.TickLabelInterpreter = 'latex';
        ha.Label.Interpreter = 'latex';
    end
end

end


