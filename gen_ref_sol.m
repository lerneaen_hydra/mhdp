function [res_ref, map_ref, cost_res] = gen_ref_sol(dp_param, model_param, cfun, res_ucpadp, N)
% Solve reference problem using DP, generate plots showing N first samples
% of state trajectory

% Modify problem set-up to solve one specific (open-loop) problem. Set
% problem horizon an order of magnitude larger the horizon found by UCPADP,
% should be more than sufficient to get a baseline solution.
dp_param.prb.N_t = res_ucpadp.N_m*10;

fprintf('Generating reference (open-loop) solution with problem horizon %d.\n', dp_param.prb.N_t);

% Round up number of grid points to nearest odd value to ensure [0;0] is
% included in DP grid
dp_param.prb.N_x_grid = floor(dp_param.prb.N_x_grid/2)*2+1;

% Grid state separation
dx = (dp_param.prb.X_h - dp_param.prb.X_l) ./ dp_param.prb.N_x_grid;

% Set initial state to start off at [0;0] state.
dp_param.prb.X0_l = [0; 0] - dx/2;
dp_param.prb.X0_h = [0; 0] + dx/2;

inp.m = model_param;
inp.arg.costfun = cfun;

% Solve problem, returning state trajectory
[res_ref, ~, ~, ~, map_ref] = dpm(dp_param, inp);
res_ref = res_ref{1};
x_ref = cat(1,res_ref(:).x);
u_ref = cat(1,res_ref(:).u);
c_ref = cat(1,res_ref.c);

% Generate state trajectory from UCPADP control law
x_ucpadp = zeros(dp_param.prb.N_t, 2);
u_ucpadp = zeros(dp_param.prb.N_t, 1);
c_ucpadp = zeros(dp_param.prb.N_t, 1);

for k=1:size(x_ucpadp, 1)-1
   u_ucpadp(k) = res_ucpadp.u_law(x_ucpadp(k,:));
   [x_nn, c_ucpadp(k)] = model(x_ucpadp(k,:), u_ucpadp(k), [], inp);
   x_ucpadp(k+1,:) = x_nn(:).';
end

% Generate plot of reference and UCPADP state/control trajectories
n = 0:N;

h = figure('visible', 'off');
clf();
fix_figure(h);
title('UCPADP solution accuracy');
for k=1:3
    subplot(3, 1, k);
    if k==1
        plot(n, x_ref(1:N+1,1), n, x_ucpadp(1:N+1,1), '--');
        str = '$\theta$';
    elseif k==2
        plot(n, x_ref(1:N+1,2), n, x_ucpadp(1:N+1,2), '--');
        str = '$\dot{\theta}$';
    else
        stairs(n, u_ref(1:N+1));
        hold on;
        stairs(n, u_ucpadp(1:N+1), '--');
        str = '$u$';
        xlabel('Sample', 'Interpreter', 'latex');
    end
    ylabel(str, 'Interpreter', 'latex');
    
    % Due to space constraints, only draw legend for first plot
    if k==1
        hl = legend({'Ref.', 'UCPADP'}, 'Location', 'East');
        fix_legend_fonts(hl);
    end
end
   
fix_figure(h, [4,2], 160);
export_fig(sprintf(['.', filesep, 'gfx', filesep, '%s_ref_vs_ucpadp_dx=%s.pdf'], dp_param.name, mat2str(dx, 3)), '-transparent');
close(h);

Solver = {'UCPADP'; 'Reference'};
Avg_Cost = [mean(c_ucpadp(1:N)); mean(c_ref(1:N))];
Avg_U_squared = [mean(u_ucpadp(1:N).^2); mean(u_ref(1:N).^2)];
cost_res.summary = table(Solver, Avg_Cost, Avg_U_squared);
cost_res.c.ucpadp = c_ucpadp;
cost_res.c.ref = c_ref;
cost_res.u.ucpadp = u_ucpadp;
cost_res.u.ref = u_ref;

end

