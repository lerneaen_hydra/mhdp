function best_p = bisection(f, a, b, tol, maxiter)
% Use bisection method to find root to some function f
% Terminates when |f(p)| < tol or after maxiter iterations (whichever is
% first)

% Usage example: find root to cos(x) in range 0 <= x <= pi, with tolerance
% 1e-6 and at most 20 interval halvings:
% x_root = bisection(@cos, 0, pi, 1e-6, 20)

% Use function memoization for convenient way to improve performance by
% removing repeated calls to function

fm = memoize(f);

% Swap a,b if ordered incorrectly
if b < a
	temp = a;
	a = b;
	b = temp;
end

% Verify input contains a sign change, if not then heuristically increase search range
while fm(a)*fm(b)>0
	p = (a + b) / 2;
	delta = b - a;
    a = p - delta;
	b = p + delta;
	fprintf('Did not find sign change in interval, testing range [%e,%e]\n', a, b);
end

% Generate new sample point
p = (a + b) / 2;

err = abs(fm(p));

best_p = p;
best_err = err;

% Quit when error is less than requested tolerance
while err > tol && maxiter > 0
	maxiter = maxiter - 1;
	% Different sign as left point -> update right point, else left point
	if fm(a) * fm(p) < 0
	   b = p;
	else
	   a = p;
	end
	
	% Generate next sample point and update error
	p = (a + b) / 2;
	err = abs(fm(p));
	
	if(err < best_err)
		best_err = err;
		best_p = p;
	end
	
	fprintf('Zero in range [%e, %e] with midpoint error %e\n', a, b, err);
end

fprintf('Found zero at %e\n', p);

end
    