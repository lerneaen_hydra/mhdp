# UCPADP example
Sample implementation of the UCPADP method. See the included paper for details on the workings of the method.

Be sure to clone/download the dpm submodule, e.g. with `git clone --recurse-submodules https://gitlab.com/lerneaen_hydra/ucpadp.git`.
Alternatively, download dpm from https://gitlab.com/lerneaen_hydra/dpm and place it in the root ucpadp directory.

Run `gen_sols.m` to generate all solutions.  

The entire workspace after running `gen_sols.m` is stored in `sols.mat`.
